# Http Errors

@comodinx/http-errors is a Node.js http errors classes.

## Index

* [Download & Install][install].
* [How is it used?][how_is_it_used].
* [Available errors][available_errors].
* [Tests][tests].

## Download & Install

### NPM
```bash
$ npm install @comodinx/http-error
```

### Source code
```bash
$ git clone https://gitlab.com/comodinx/http-error.git
$ cd http-error
$ npm install
```

## How is it used?

```js
'use strict';

const errors = require('@comodinx/http-error');
// ...
throw new errors.NotFound();
```

```js
// All HTTP Status Code are an Exception
const e = new errors.NotFound('My custom message', { hello: 'world' });
// ...
console.log(e.toJson());
// { "error": 'My custom message', "code": 404, "extra": { "hello": "world" } }
```


## Available errors
 * Continue
 * SwitchingProtocols
 * Processing
 * Ok
 * Created
 * Accepted
 * NonAuthoritativeInformation
 * NoContent
 * ResetContent
 * PartialContent
 * MultiStatus
 * AlreadyReported
 * ImUsed
 * MultipleChoices
 * MovedPermanently
 * Found
 * SeeOther
 * NotModified
 * UseProxy
 * SwitchProxy
 * TemporaryRedirect
 * PermanentRedirect
 * ResumeIncomplete
 * BadRequest
 * Unauthorized
 * PaymentRequired
 * Forbidden
 * NotFound
 * MethodNotAllowed
 * NotAcceptable
 * ProxyAuthenticationRequired
 * RequestTimeout
 * Conflict
 * Gone
 * LengthRequired
 * PreconditionFailed
 * PayloadTooLarge
 * UriTooLong
 * UnsupportedMediaType
 * RangeNotSatisfiable
 * ExpectationFailed
 * ImATeapot
 * AuthenticationTimeout
 * MethodFailure
 * EnhanceYourCalm
 * MisdirectedRequest
 * UnprocessableEntity
 * Locked
 * FailedDependency
 * UpgradeRequired
 * PreconditionRequired
 * TooManyRequests
 * RequestHeaderFieldsTooLarge
 * LoginTimeout
 * NoResponse
 * RetryWith
 * BlockedByWindowsParentalControls
 * UnavailableForLegalReasons
 * Redirect
 * RequestHeaderTooLarge
 * CertError
 * NoCert
 * HttpToHttps
 * TokenExpiredInvalid
 * ClientClosedRequest
 * TokenRequired
 * InternalServerError
 * NotImplemented
 * BadGateway
 * ServiceUnavailable
 * GatewayTimeout
 * HttpVersionNotSupported
 * VariantAlsoNegotiates
 * InsufficientStorage
 * LoopDetected
 * BandwidthLimitExceeded
 * NotExtended
 * NetworkAuthenticationRequired
 * UnknownError
 * OriginConnectionTimeout
 * NetworkReadTimeout
 * NetworkConnectTimeoutError
 * JsonErrorResponse
 * CustomError

## Tests

In order to see more concrete examples, **I INVITE YOU TO LOOK AT THE TESTS :)**

### Run the unit tests
```sh
npm test
```

### Run an application (server) with a more formal example.
```sh
npm run test-app
```

<!-- deep links -->
[install]: #download--install
[how_is_it_used]: #how-is-it-used
[available_errors]: #available-errors
[tests]: #tests
