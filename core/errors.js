'use strict';

const http = require('http-constants');
const CustomError = require('./custom');

class Continue extends CustomError {
    constructor (message = 'Continue', extra) {
        super(message, http.codes.CONTINUE, extra);
    }

    static get code () {
        return http.codes.CONTINUE;
    }
}

class SwitchingProtocols extends CustomError {
    constructor (message = 'Switching Protocols', extra) {
        super(message, http.codes.SWITCHING_PROTOCOLS, extra);
    }

    static get code () {
        return http.codes.SWITCHING_PROTOCOLS;
    }
}

class Processing extends CustomError {
    constructor (message = 'Processing', extra) {
        super(message, http.codes.PROCESSING, extra);
    }

    static get code () {
        return http.codes.PROCESSING;
    }
}

class Ok extends CustomError {
    constructor (message = 'Ok', extra) {
        super(message, http.codes.OK, extra);
    }

    static get code () {
        return http.codes.OK;
    }
}

class Created extends CustomError {
    constructor (message = 'Created', extra) {
        super(message, http.codes.CREATED, extra);
    }

    static get code () {
        return http.codes.CREATED;
    }
}

class Accepted extends CustomError {
    constructor (message = 'Accepted', extra) {
        super(message, http.codes.ACCEPTED, extra);
    }

    static get code () {
        return http.codes.ACCEPTED;
    }
}

class NonAuthoritativeInformation extends CustomError {
    constructor (message = 'Non Authoritative Information', extra) {
        super(message, http.codes.NON_AUTHORITATIVE_INFORMATION, extra);
    }

    static get code () {
        return http.codes.NON_AUTHORITATIVE_INFORMATION;
    }
}

class NoContent extends CustomError {
    constructor (message = 'No Content', extra) {
        super(message, http.codes.NO_CONTENT, extra);
    }

    static get code () {
        return http.codes.NO_CONTENT;
    }
}

class ResetContent extends CustomError {
    constructor (message = 'Reset Content', extra) {
        super(message, http.codes.RESET_CONTENT, extra);
    }

    static get code () {
        return http.codes.RESET_CONTENT;
    }
}

class PartialContent extends CustomError {
    constructor (message = 'Partial Content', extra) {
        super(message, http.codes.PARTIAL_CONTENT, extra);
    }

    static get code () {
        return http.codes.PARTIAL_CONTENT;
    }
}

class MultiStatus extends CustomError {
    constructor (message = 'Multi Status', extra) {
        super(message, http.codes.MULTI_STATUS, extra);
    }

    static get code () {
        return http.codes.MULTI_STATUS;
    }
}

class AlreadyReported extends CustomError {
    constructor (message = 'Already Reported', extra) {
        super(message, http.codes.ALREADY_REPORTED, extra);
    }

    static get code () {
        return http.codes.ALREADY_REPORTED;
    }
}

class ImUsed extends CustomError {
    constructor (message = 'I\'m Used', extra) {
        super(message, http.codes.IM_USED, extra);
    }

    static get code () {
        return http.codes.IM_USED;
    }
}

class MultipleChoices extends CustomError {
    constructor (message = 'Multiple Choices', extra) {
        super(message, http.codes.MULTIPLE_CHOICES, extra);
    }

    static get code () {
        return http.codes.MULTIPLE_CHOICES;
    }
}

class MovedPermanently extends CustomError {
    constructor (message = 'Moved Permanently', extra) {
        super(message, http.codes.MOVED_PERMANENTLY, extra);
    }

    static get code () {
        return http.codes.MOVED_PERMANENTLY;
    }
}

class Found extends CustomError {
    constructor (message = 'Found', extra) {
        super(message, http.codes.FOUND, extra);
    }

    static get code () {
        return http.codes.FOUND;
    }
}

class SeeOther extends CustomError {
    constructor (message = 'See Other', extra) {
        super(message, http.codes.SEE_OTHER, extra);
    }

    static get code () {
        return http.codes.SEE_OTHER;
    }
}

class NotModified extends CustomError {
    constructor (message = 'Not Modified', extra) {
        super(message, http.codes.NOT_MODIFIED, extra);
    }

    static get code () {
        return http.codes.NOT_MODIFIED;
    }
}

class UseProxy extends CustomError {
    constructor (message = 'Use Proxy', extra) {
        super(message, http.codes.USE_PROXY, extra);
    }

    static get code () {
        return http.codes.USE_PROXY;
    }
}

class SwitchProxy extends CustomError {
    constructor (message = 'Switch Proxy', extra) {
        super(message, http.codes.SWITCH_PROXY, extra);
    }

    static get code () {
        return http.codes.SWITCH_PROXY;
    }
}

class TemporaryRedirect extends CustomError {
    constructor (message = 'Temporary Redirect', extra) {
        super(message, http.codes.TEMPORARY_REDIRECT, extra);
    }

    static get code () {
        return http.codes.TEMPORARY_REDIRECT;
    }
}

class PermanentRedirect extends CustomError {
    constructor (message = 'Permanent Redirect', extra) {
        super(message, http.codes.PERMANENT_REDIRECT, extra);
    }

    static get code () {
        return http.codes.PERMANENT_REDIRECT;
    }
}

class ResumeIncomplete extends CustomError {
    constructor (message = 'Resume Incomplete', extra) {
        super(message, http.codes.RESUME_INCOMPLETE, extra);
    }

    static get code () {
        return http.codes.RESUME_INCOMPLETE;
    }
}

class BadRequest extends CustomError {
    constructor (message = 'Bad Request', extra) {
        super(message, http.codes.BAD_REQUEST, extra);
    }

    static get code () {
        return http.codes.BAD_REQUEST;
    }
}

class Unauthorized extends CustomError {
    constructor (message = 'Unauthorized', extra) {
        super(message, http.codes.UNAUTHORIZED, extra);
    }

    static get code () {
        return http.codes.UNAUTHORIZED;
    }
}

class PaymentRequired extends CustomError {
    constructor (message = 'Payment Required', extra) {
        super(message, http.codes.PAYMENT_REQUIRED, extra);
    }

    static get code () {
        return http.codes.PAYMENT_REQUIRED;
    }
}

class Forbidden extends CustomError {
    constructor (message = 'Forbidden', extra) {
        super(message, http.codes.FORBIDDEN, extra);
    }

    static get code () {
        return http.codes.FORBIDDEN;
    }
}

class NotFound extends CustomError {
    constructor (message = 'Not Found', extra) {
        super(message, http.codes.NOT_FOUND, extra);
    }

    static get code () {
        return http.codes.NOT_FOUND;
    }
}

class MethodNotAllowed extends CustomError {
    constructor (message = 'Method Not Allowed', extra) {
        super(message, http.codes.METHOD_NOT_ALLOWED, extra);
    }

    static get code () {
        return http.codes.METHOD_NOT_ALLOWED;
    }
}

class NotAcceptable extends CustomError {
    constructor (message = 'Not Acceptable', extra) {
        super(message, http.codes.NOT_ACCEPTABLE, extra);
    }

    static get code () {
        return http.codes.NOT_ACCEPTABLE;
    }
}

class ProxyAuthenticationRequired extends CustomError {
    constructor (message = 'Proxy Authentication Required', extra) {
        super(message, http.codes.PROXY_AUTHENTICATION_REQUIRED, extra);
    }

    static get code () {
        return http.codes.PROXY_AUTHENTICATION_REQUIRED;
    }
}

class RequestTimeout extends CustomError {
    constructor (message = 'Request Timeout', extra) {
        super(message, http.codes.REQUEST_TIMEOUT, extra);
    }

    static get code () {
        return http.codes.REQUEST_TIMEOUT;
    }
}

class Conflict extends CustomError {
    constructor (message = 'Conflict', extra) {
        super(message, http.codes.CONFLICT, extra);
    }

    static get code () {
        return http.codes.CONFLICT;
    }
}

class Gone extends CustomError {
    constructor (message = 'Gone', extra) {
        super(message, http.codes.GONE, extra);
    }

    static get code () {
        return http.codes.GONE;
    }
}

class LengthRequired extends CustomError {
    constructor (message = 'Length Required', extra) {
        super(message, http.codes.LENGTH_REQUIRED, extra);
    }

    static get code () {
        return http.codes.LENGTH_REQUIRED;
    }
}

class PreconditionFailed extends CustomError {
    constructor (message = 'Precondition Failed', extra) {
        super(message, http.codes.PRECONDITION_FAILED, extra);
    }

    static get code () {
        return http.codes.PRECONDITION_FAILED;
    }
}

class PayloadTooLarge extends CustomError {
    constructor (message = 'Payload TooLarge', extra) {
        super(message, http.codes.PAYLOAD_TOO_LARGE, extra);
    }

    static get code () {
        return http.codes.PAYLOAD_TOO_LARGE;
    }
}

class UriTooLong extends CustomError {
    constructor (message = 'Uri Too Long', extra) {
        super(message, http.codes.URI_TOO_LONG, extra);
    }

    static get code () {
        return http.codes.URI_TOO_LONG;
    }
}

class UnsupportedMediaType extends CustomError {
    constructor (message = 'Unsupported Media Type', extra) {
        super(message, http.codes.UNSUPPORTED_MEDIA_TYPE, extra);
    }

    static get code () {
        return http.codes.UNSUPPORTED_MEDIA_TYPE;
    }
}

class RangeNotSatisfiable extends CustomError {
    constructor (message = 'Range Not Satisfiable', extra) {
        super(message, http.codes.RANGE_NOT_SATISFIABLE, extra);
    }

    static get code () {
        return http.codes.RANGE_NOT_SATISFIABLE;
    }
}

class ExpectationFailed extends CustomError {
    constructor (message = 'Expectation Failed', extra) {
        super(message, http.codes.EXPECTATION_FAILED, extra);
    }

    static get code () {
        return http.codes.EXPECTATION_FAILED;
    }
}

class ImATeapot extends CustomError {
    constructor (message = 'I\'m a Teapot', extra) {
        super(message, http.codes.IM_A_TEAPOT, extra);
    }

    static get code () {
        return http.codes.IM_A_TEAPOT;
    }
}

class AuthenticationTimeout extends CustomError {
    constructor (message = 'Authentication Timeout', extra) {
        super(message, http.codes.AUTHENTICATION_TIMEOUT, extra);
    }

    static get code () {
        return http.codes.AUTHENTICATION_TIMEOUT;
    }
}

class MethodFailure extends CustomError {
    constructor (message = 'Method Failure', extra) {
        super(message, http.codes.METHOD_FAILURE, extra);
    }

    static get code () {
        return http.codes.METHOD_FAILURE;
    }
}

class EnhanceYourCalm extends CustomError {
    constructor (message = 'Enhance Your Calm', extra) {
        super(message, http.codes.ENHANCE_YOUR_CALM, extra);
    }

    static get code () {
        return http.codes.ENHANCE_YOUR_CALM;
    }
}

class MisdirectedRequest extends CustomError {
    constructor (message = 'Misdirected Request', extra) {
        super(message, http.codes.MISDIRECTED_REQUEST, extra);
    }

    static get code () {
        return http.codes.MISDIRECTED_REQUEST;
    }
}

class UnprocessableEntity extends CustomError {
    constructor (message = 'Unprocessable Entity', extra) {
        super(message, http.codes.UNPROCESSABLE_ENTITY, extra);
    }

    static get code () {
        return http.codes.UNPROCESSABLE_ENTITY;
    }
}

class Locked extends CustomError {
    constructor (message = 'Locked', extra) {
        super(message, http.codes.LOCKED, extra);
    }

    static get code () {
        return http.codes.LOCKED;
    }
}

class FailedDependency extends CustomError {
    constructor (message = 'Failed Dependency', extra) {
        super(message, http.codes.FAILED_DEPENDENCY, extra);
    }

    static get code () {
        return http.codes.FAILED_DEPENDENCY;
    }
}

class UpgradeRequired extends CustomError {
    constructor (message = 'Upgrade Required', extra) {
        super(message, http.codes.UPGRADE_REQUIRED, extra);
    }

    static get code () {
        return http.codes.UPGRADE_REQUIRED;
    }
}

class PreconditionRequired extends CustomError {
    constructor (message = 'Precondition Required', extra) {
        super(message, http.codes.PRECONDITION_REQUIRED, extra);
    }

    static get code () {
        return http.codes.PRECONDITION_REQUIRED;
    }
}

class TooManyRequests extends CustomError {
    constructor (message = 'Too Many Requests', extra) {
        super(message, http.codes.TOO_MANY_REQUESTS, extra);
    }

    static get code () {
        return http.codes.TOO_MANY_REQUESTS;
    }
}

class RequestHeaderFieldsTooLarge extends CustomError {
    constructor (message = 'Request Header Fields Too Large', extra) {
        super(message, http.codes.REQUEST_HEADER_FIELDS_TOO_LARGE, extra);
    }

    static get code () {
        return http.codes.REQUEST_HEADER_FIELDS_TOO_LARGE;
    }
}

class LoginTimeout extends CustomError {
    constructor (message = 'Login Timeout', extra) {
        super(message, http.codes.LOGIN_TIMEOUT, extra);
    }

    static get code () {
        return http.codes.LOGIN_TIMEOUT;
    }
}

class NoResponse extends CustomError {
    constructor (message = 'No Response', extra) {
        super(message, http.codes.NO_RESPONSE, extra);
    }

    static get code () {
        return http.codes.NO_RESPONSE;
    }
}

class RetryWith extends CustomError {
    constructor (message = 'Retry With', extra) {
        super(message, http.codes.RETRY_WITH, extra);
    }

    static get code () {
        return http.codes.RETRY_WITH;
    }
}

class BlockedByWindowsParentalControls extends CustomError {
    constructor (message = 'Blocked By Windows Parental Controls', extra) {
        super(message, http.codes.BLOCKED_BY_WINDOWS_PARENTAL_CONTROLS, extra);
    }

    static get code () {
        return http.codes.BLOCKED_BY_WINDOWS_PARENTAL_CONTROLS;
    }
}

class UnavailableForLegalReasons extends CustomError {
    constructor (message = 'Unavailable For Legal Reasons', extra) {
        super(message, http.codes.UNAVAILABLE_FOR_LEGAL_REASONS, extra);
    }

    static get code () {
        return http.codes.UNAVAILABLE_FOR_LEGAL_REASONS;
    }
}

class Redirect extends CustomError {
    constructor (message = 'Redirect', extra) {
        super(message, http.codes.REDIRECT, extra);
    }

    static get code () {
        return http.codes.REDIRECT;
    }
}

class RequestHeaderTooLarge extends CustomError {
    constructor (message = 'Request Header Too Large', extra) {
        super(message, http.codes.REQUEST_HEADER_TOO_LARGE, extra);
    }

    static get code () {
        return http.codes.REQUEST_HEADER_TOO_LARGE;
    }
}

class CertError extends CustomError {
    constructor (message = 'Cert Error', extra) {
        super(message, http.codes.CERT_ERROR, extra);
    }

    static get code () {
        return http.codes.CERT_ERROR;
    }
}

class NoCert extends CustomError {
    constructor (message = 'No Cert', extra) {
        super(message, http.codes.NO_CERT, extra);
    }

    static get code () {
        return http.codes.NO_CERT;
    }
}

class HttpToHttps extends CustomError {
    constructor (message = 'Http To Https', extra) {
        super(message, http.codes.HTTP_TO_HTTPS, extra);
    }

    static get code () {
        return http.codes.HTTP_TO_HTTPS;
    }
}

class TokenExpiredInvalid extends CustomError {
    constructor (message = 'Token Expired Invalid', extra) {
        super(message, http.codes.TOKEN_EXPIRED_INVALID, extra);
    }

    static get code () {
        return http.codes.TOKEN_EXPIRED_INVALID;
    }
}

class ClientClosedRequest extends CustomError {
    constructor (message = 'Client Closed Request', extra) {
        super(message, http.codes.CLIENT_CLOSED_REQUEST, extra);
    }

    static get code () {
        return http.codes.CLIENT_CLOSED_REQUEST;
    }
}

class TokenRequired extends CustomError {
    constructor (message = 'Token Required', extra) {
        super(message, http.codes.TOKEN_REQUIRED, extra);
    }

    static get code () {
        return http.codes.TOKEN_REQUIRED;
    }
}

class InternalServerError extends CustomError {
    constructor (message = 'Internal Server Error', extra) {
        super(message, http.codes.INTERNAL_SERVER_ERROR, extra);
    }

    static get code () {
        return http.codes.INTERNAL_SERVER_ERROR;
    }
}

class NotImplemented extends CustomError {
    constructor (message = 'Not Implemented', extra) {
        super(message, http.codes.NOT_IMPLEMENTED, extra);
    }

    static get code () {
        return http.codes.NOT_IMPLEMENTED;
    }
}

class BadGateway extends CustomError {
    constructor (message = 'Bad Gateway', extra) {
        super(message, http.codes.BAD_GATEWAY, extra);
    }

    static get code () {
        return http.codes.BAD_GATEWAY;
    }
}

class ServiceUnavailable extends CustomError {
    constructor (message = 'Service Unavailable', extra) {
        super(message, http.codes.SERVICE_UNAVAILABLE, extra);
    }

    static get code () {
        return http.codes.SERVICE_UNAVAILABLE;
    }
}

class GatewayTimeout extends CustomError {
    constructor (message = 'Gateway Timeout', extra) {
        super(message, http.codes.GATEWAY_TIMEOUT, extra);
    }

    static get code () {
        return http.codes.GATEWAY_TIMEOUT;
    }
}

class HttpVersionNotSupported extends CustomError {
    constructor (message = 'Http Version Not Supported', extra) {
        super(message, http.codes.HTTP_VERSION_NOT_SUPPORTED, extra);
    }

    static get code () {
        return http.codes.HTTP_VERSION_NOT_SUPPORTED;
    }
}

class VariantAlsoNegotiates extends CustomError {
    constructor (message = 'Variant Also Negotiates', extra) {
        super(message, http.codes.VARIANT_ALSO_NEGOTIATES, extra);
    }

    static get code () {
        return http.codes.VARIANT_ALSO_NEGOTIATES;
    }
}

class InsufficientStorage extends CustomError {
    constructor (message = 'Insufficient Storage', extra) {
        super(message, http.codes.INSUFFICIENT_STORAGE, extra);
    }

    static get code () {
        return http.codes.INSUFFICIENT_STORAGE;
    }
}

class LoopDetected extends CustomError {
    constructor (message = 'Loop Detected', extra) {
        super(message, http.codes.LOOP_DETECTED, extra);
    }

    static get code () {
        return http.codes.LOOP_DETECTED;
    }
}

class BandwidthLimitExceeded extends CustomError {
    constructor (message = 'Bandwidth Limit Exceeded', extra) {
        super(message, http.codes.BANDWIDTH_LIMIT_EXCEEDED, extra);
    }

    static get code () {
        return http.codes.BANDWIDTH_LIMIT_EXCEEDED;
    }
}

class NotExtended extends CustomError {
    constructor (message = 'Not Extended', extra) {
        super(message, http.codes.NOT_EXTENDED, extra);
    }

    static get code () {
        return http.codes.NOT_EXTENDED;
    }
}

class NetworkAuthenticationRequired extends CustomError {
    constructor (message = 'Network Authentication Required', extra) {
        super(message, http.codes.NETWORK_AUTHENTICATION_REQUIRED, extra);
    }

    static get code () {
        return http.codes.NETWORK_AUTHENTICATION_REQUIRED;
    }
}

class UnknownError extends CustomError {
    constructor (message = 'Unknown Error', extra) {
        super(message, http.codes.UNKNOWN_ERROR, extra);
    }

    static get code () {
        return http.codes.UNKNOWN_ERROR;
    }
}

class OriginConnectionTimeout extends CustomError {
    constructor (message = 'Origin Connection Timeout', extra) {
        super(message, http.codes.ORIGIN_CONNECTION_TIMEOUT, extra);
    }

    static get code () {
        return http.codes.ORIGIN_CONNECTION_TIMEOUT;
    }
}

class NetworkReadTimeout extends CustomError {
    constructor (message = 'Network Read Timeout', extra) {
        super(message, http.codes.NETWORK_READ_TIMEOUT, extra);
    }

    static get code () {
        return http.codes.NETWORK_READ_TIMEOUT;
    }
}

class NetworkConnectTimeoutError extends CustomError {
    constructor (message = 'Network Connect Timeout Error', extra) {
        super(message, http.codes.NETWORK_CONNECT_TIMEOUT_ERROR, extra);
    }

    static get code () {
        return http.codes.NETWORK_CONNECT_TIMEOUT_ERROR;
    }
}

class JsonErrorResponse extends CustomError {
    constructor (json, status = http.codes.INTERNAL_SERVER_ERROR, message = 'Json Error Response') {
        super(message, status);
        this.extra = json;
    }

    toJson () {
        return this.extra;
    }
}

module.exports = {
    Continue,
    SwitchingProtocols,
    Processing,
    Ok,
    Created,
    Accepted,
    NonAuthoritativeInformation,
    NoContent,
    ResetContent,
    PartialContent,
    MultiStatus,
    AlreadyReported,
    ImUsed,
    MultipleChoices,
    MovedPermanently,
    Found,
    SeeOther,
    NotModified,
    UseProxy,
    SwitchProxy,
    TemporaryRedirect,
    PermanentRedirect,
    ResumeIncomplete,
    BadRequest,
    Unauthorized,
    PaymentRequired,
    Forbidden,
    NotFound,
    MethodNotAllowed,
    NotAcceptable,
    ProxyAuthenticationRequired,
    RequestTimeout,
    Conflict,
    Gone,
    LengthRequired,
    PreconditionFailed,
    PayloadTooLarge,
    UriTooLong,
    UnsupportedMediaType,
    RangeNotSatisfiable,
    ExpectationFailed,
    ImATeapot,
    AuthenticationTimeout,
    MethodFailure,
    EnhanceYourCalm,
    MisdirectedRequest,
    UnprocessableEntity,
    Locked,
    FailedDependency,
    UpgradeRequired,
    PreconditionRequired,
    TooManyRequests,
    RequestHeaderFieldsTooLarge,
    LoginTimeout,
    NoResponse,
    RetryWith,
    BlockedByWindowsParentalControls,
    UnavailableForLegalReasons,
    Redirect,
    RequestHeaderTooLarge,
    CertError,
    NoCert,
    HttpToHttps,
    TokenExpiredInvalid,
    ClientClosedRequest,
    TokenRequired,
    InternalServerError,
    NotImplemented,
    BadGateway,
    ServiceUnavailable,
    GatewayTimeout,
    HttpVersionNotSupported,
    VariantAlsoNegotiates,
    InsufficientStorage,
    LoopDetected,
    BandwidthLimitExceeded,
    NotExtended,
    NetworkAuthenticationRequired,
    UnknownError,
    OriginConnectionTimeout,
    NetworkReadTimeout,
    NetworkConnectTimeoutError,
    JsonErrorResponse,
    CustomError
};
