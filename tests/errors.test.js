'use strict';

const http = require('http-constants');
const errors = require('../core');

describe('@comodinx/errors', () => {
    Object.keys(errors).forEach(errorName => {
        const ErrorClass = errors[errorName];

        if (!ErrorClass.code) {
            return;
        }

        describe(errorName, () => {
            it(`should return code/status ${ErrorClass.code}`, done => {
                const result = new ErrorClass(errorName);

                expect(result).to.have.property('code').to.be.a('number').equal(ErrorClass.code);
                expect(result).to.have.property('message').to.be.a('string').equal(errorName);
                done();
            });
        });
    });

    describe('JsonErrorResponse', () => {
        it(`should return code/status ${http.codes.INTERNAL_SERVER_ERROR}`, done => {
            const data = { a: 'b', c: 1 };
            const result = new errors.JsonErrorResponse(data);

            expect(result).to.have.property('code').to.be.a('number').equal(http.codes.INTERNAL_SERVER_ERROR);
            expect(result).to.have.property('message').to.be.a('string').equal('Json Error Response');
            expect(result.toJson()).to.have.property('a').to.be.a('string').equal(data.a);

            done();
        });
    });

    describe('CustomError', () => {
        it(`should return code/status ${http.codes.INTERNAL_SERVER_ERROR}`, done => {
            const result = new errors.CustomError('Custom Error');

            expect(result).to.have.property('code').to.be.a('number').equal(http.codes.INTERNAL_SERVER_ERROR);
            expect(result).to.have.property('message').to.be.a('string').equal('Custom Error');

            done();
        });
    });
});
